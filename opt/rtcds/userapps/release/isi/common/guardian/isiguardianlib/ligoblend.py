from ezca.ligofilter import LIGOFilter
from ezca.ezcaPV import EzcaPV
from collections import namedtuple
import time
import const

class LIGOBlendError(Exception): pass

_no_value = object()


MAX_FILTERS = 10
NUM_FILTER_ID_DIGITS = 2
BLEND_READ_TIME_STEP = 0.125  # seconds
BLEND_SLOTS = ['CUR', 'NXT']
BLEND_FILTER_CHANNEL_NAME = 'BLND_{deg_of_free}_{suffix}'
BLEND_NAMES_SENSOR = 'CPS'
MIX_READ_TIME_STEP = 0.125
BLEND_FILTER_SENSOR_NAMES = dict(
        BSC_ST1 = ('CPS', 'T240', 'L4C'),
        BSC_ST2 = ('CPS', 'GS13'),
        HAM     = ('CPS', 'GS13'),
    )

# Blend times are changed, FM9/10 are 120s, FM1-8 are 60
#BLENDING_TIME = 120
def BLENDING_TIME(next_filter):
    """
    Seconds determined from the c code
    ST1 60s for FM1-8, 120s for FM9-10
    ST2 all 20s
    """
    if 'ST1' in const.CHAMBER_TYPE:
        return 60 if next_filter not in [9,10] else 120
    else:
        return 20

# Define Blend class with method names that make more sense
# in a blend filter context.
from ezca.ramp import Ramp
class Blend(Ramp):
    start_blend_transition = Ramp.start_ramp
    is_mixing = Ramp.is_ramping


# FIXME need wrapper to check blend_slot and deg_of_free arguments
class LIGOBlendManager(object):
    """SEI EPICS interface to a standard set of blend filters.

    'deg_of_free_list' is a list of degrees of freedom to manage the blend
    filters of.

    'ezca' is an instantiated ezca object which will be used for
    all of the channel access reads and writes that occur in this class.

    The LIGOBlendManager manages the blend filters corresponding to the
    degrees of freedom in <deg_of_free_list> whose names begin with
    <ezca>.prefix + '_BLND'. An instantiation of this class can transition
    all of the blend filters of the managed degrees of freedom all
    at once, and also report whether a transition to a new blend filter
    is occurring. The instantiation will also have access to the name
    of the blend filters installed for each degree of freedom.

    Note that the LIGOBlendManager does not support transitioning different
    degrees of freedom to different blend filters. If that is what you want,
    then instantiate more than LIGOBlendManager.
    """
    def __init__(self, deg_of_free_list, ezca):
        self.sensor_names = BLEND_FILTER_SENSOR_NAMES[const.CHAMBER_TYPE]
        self.ezca = ezca
        self.deg_of_free_list = deg_of_free_list

        # instantiate LIGOFilter for each filter in the blend filter bank 
        self.__filter_dict = dict()
        for _deg_of_free in self.deg_of_free_list:
            # instantiate EzcaPVs for blend filter DESIRED_FM channels for each
            # degree of freedom
            for suffix in ['DESIRED_FM']:
                setattr(self, _deg_of_free+'_'+suffix,
                        EzcaPV(BLEND_FILTER_CHANNEL_NAME.format(deg_of_free=_deg_of_free, suffix=suffix),
                                ezca=self.ezca))

            for _sensor_name in self.sensor_names:
                # Instantiate LIGOFilter objects for each blend slot associated with each sensor
                # and degree of freedom.
                for blend_slot in BLEND_SLOTS:
                    self.__filter_dict['_'.join([_deg_of_free, _sensor_name, blend_slot])] =\
                            LIGOFilter(BLEND_FILTER_CHANNEL_NAME.format(
                                    deg_of_free=_deg_of_free,
                                    suffix=_sensor_name)+'_'+blend_slot, self.ezca)

            # Instantiate Blend objects for transitioning blend filter and querying
            # whether a blend filter is still mixing.
            # This is the main code that switches the blends
            def start_blend_transition(nxt_blend_number, _deg_of_free=_deg_of_free):
                # Move the offset of the CUR blend filter to the NXT filter
                # and then turn on the NXT blend filter.
                for _sensor_name in self.sensor_names:
                    cur_filter_offset = self.__filter_dict['_'.join([_deg_of_free, _sensor_name, 'CUR'])].OFFSET.get()
                    nxt_filter = self.__filter_dict['_'.join([_deg_of_free, _sensor_name, 'NXT'])]
                    nxt_filter.OFFSET.put(cur_filter_offset, wait=True)
                    nxt_filter.only_on('INPUT','OUTPUT','OFFSET','DECIMATE', wait=True)
            
                # Changed the value of DESIRED_FM to start the transition. After DESIRED_FM
                # is changed, the front-end has control of the blend filters!
                DESIRED_FM = getattr(self,'_'.join([_deg_of_free,'DESIRED_FM']))
                DESIRED_FM.put(nxt_blend_number, wait=True)
            
            def is_mixing(value):
                return bool(value)
            
            # Set particular attributes to the label of "<deg_of_free>_blend" from __filter_dict.
            # This attribute can be called and acted on by either is_mixing or start_blend_transition
            # Example: if you get the attribute of "X_blend" and call the is_mixing method on it,
            # this will automatically apply the appropriate channel name to that method.
            # blend = getattr(self, '_'.join(['X', 'blend']))
            # blend.is_mixing()
            setattr(self, '_'.join([_deg_of_free, 'blend']),
                    Blend(BLEND_FILTER_CHANNEL_NAME.format(deg_of_free=_deg_of_free, suffix='MIXSTATE'),
                            is_mixing,
                            start_blend_transition,
                            self.ezca))

    def __repr__(self):
        return '%s(deg_of_free_list=%r, ezca=%r)' % \
                (self.__class__.__name__, self.deg_of_free_list, self.ezca)

    def transition_all_blends(self, nxt_blend_number, wait=True):
        """
        Transitions all of the managed degrees of freedom to use the blend
        filter identified by <nxt_blend_number>. Note that <nxt_blend_number>
        is 1-indexed.
        """
        for deg_of_free in self.deg_of_free_list:
            # Use the get_names method to make sure that there is a filter installed in the desired blend slot.
            blend_filter_names = self.get_names(deg_of_free)
            blend_filter_name = blend_filter_names[nxt_blend_number-1]
            if not blend_filter_name:
                raise LIGOBlendError("No blend filter installed in FM%d for the %s degree of freedom."\
                        % (nxt_blend_number, deg_of_free))
            log('Transitioning BLEND_%s to %s'\
                % (deg_of_free, blend_filter_name))
            # Obtain the attribute that start_blend_transtion will act on
            blend = getattr(self, '_'.join([deg_of_free, 'blend']))
            if not blend.is_mixing():
                blend.start_blend_transition(nxt_blend_number)
            else:
                continue

        if wait:
            # Sleep for a time to allow the trasition form the NXT to CUR filter banks.
            time.sleep(BLENDING_TIME(nxt_blend_number))
            # Make sure that it is done
            self.wait_for_all_mixing_to_finish()
            # Disengage the NXT filter since the CUR filter now has the desired settings
            self.complete_blend_transition()

    def wait_for_all_mixing_to_finish(self):
        """
        Blocks until all of the managed degrees of freedom are no longer mixing
        the output of two blend filters.
        """
        while True:
            if self.is_transitioning_blends():
                time.sleep(MIX_READ_TIME_STEP)
                continue
            break

    def complete_blend_transition(self):
        """
        Disengages the NXT blend filter. If wait=False is passed to
        transition_all_blends(), then this method must be run to complete the
        transition to the new blend filter.
        """
        for sensor_name in self.sensor_names:
            for deg_of_free in self.deg_of_free_list:
                nxt_filter = self.__filter_dict['_'.join([deg_of_free, sensor_name, 'NXT'])]
                nxt_filter.all_off(wait=True)

    def is_transitioning_blends(self):
        """
        Returns whether one of the blend filters for the managed degrees of
        freedom is transitioning to a different blend filter.
        """
        for deg_of_free in self.deg_of_free_list:
            blend = getattr(self, '_'.join([deg_of_free, 'blend']))
            if blend.is_mixing():
                return True
        return False

    def get_names(self, deg_of_free):
        """
        Returns a list of the names of the blend filters installed on the
        <deg_of_free> blend filter.
        """
        names = []
        for i in range(MAX_FILTERS):
            i = ('{:0'+str(NUM_FILTER_ID_DIGITS)+'d}').format(i)
            names.append(self.ezca.read(BLEND_FILTER_CHANNEL_NAME.format(deg_of_free=deg_of_free, suffix=BLEND_NAMES_SENSOR)+'_CUR_Name'+i))
        return names

    def get_cur_blend_name(self, deg_of_free):
        """
        Returns a tuple containing the number and name, respectively, of the current filter.
        """
        for i in range(MAX_FILTERS):
            if self.__filter_dict['_'.join([deg_of_free, BLEND_NAMES_SENSOR, 'CUR'])].is_engaged(i+1):
                return i+1, self.get_names(deg_of_free)[i]
        raise LIGOBlendError('There are no blend filters engaged for '+deg_of_free+' degree of freedom.')

    def get_nxt_blend_name(self, deg_of_free):
        """
        Returns a tuple containing the number and name of the next filter,
        respectively. If the NXT blend filter is not engaged, then this method
        returns None.
        """
        fltr = self.__filter_dict['_'.join([deg_of_free, BLEND_NAMES_SENSOR, 'NXT'])]
        if not (fltr.is_engaged('OUTPUT') and fltr.is_engaged('INPUT')):
            return None

        for i in range(MAX_FILTERS):
            if fltr.is_engaged(i+1):
                return i+1, self.get_names(deg_of_free)[i]
