# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState, GuardStateDecorator, NodeManager
from ezca.ligofilter import LIGOFilter
from isiguardianlib.ligoblend import LIGOBlendManager
from isiguardianlib.blend import states as blend_states
from isiguardianlib.blend import const as blend_const
from isiguardianlib.sensor_correction import states as sc_states
from isiguardianlib import const as top_const
import const
import re
######################################

SC_ramp_time = 5

##################################################
# Configuration Control
class desired_conf():
    """This will make it a bit more clear what configuration and filter 
    we are calling when writing new states. The purpose was to allow for 
    easy state creation. Hopefully it makes it easier and not more 
    confusing..

    Sample use:
    >>> A = desired_conf('SC', 'A')
    >>> A.Filter_Banks
    ['WNR','FIR']
    >>> A.MATCH_X
    [2,3]
    """
    def __init__(self, BLENDorSC, conf_letter):
        if BLENDorSC == 'BLEND':
            self.blend = True
            self.SC = False
            self.filter_dict = const.BLEND_FILTER_SELECTION
        elif BLENDorSC == 'SC':
            self.SC = True
            self.blend = False
            self.filter_dict = const.SC_FM_confs
        self.conf = conf_letter
        # Run to set attributes
        self.bank_dof()
    @property
    def Filter_Banks(self):
        #return [bank for bank in self.filter_dict[self.conf]]
        banks = [bank for bank in self.filter_dict[self.conf]]
        for bank in banks:
            total_dofs = 0
            for dof in self.filter_dict[self.conf][bank]:
                if self.filter_dict[self.conf][bank][dof]:
                    total_dofs +=1
            if total_dofs == 0:
                banks.remove(bank)
        return banks
    @property
    def Blend_dofs(self):
        if self.blend:
            return [dof for dof in self.filter_dict[self.conf]['BLEND'] \
                        if self.filter_dict[self.conf]['BLEND'][dof]]
        else:
            return None
    @property
    def SC_dofs(self):
        if self.SC:
            SCdict = {}
            for bank in self.Filter_Banks:
                SCdict[bank] = [dof for dof in self.filter_dict[self.conf][bank] \
                        if self.filter_dict[self.conf][bank][dof]]
            return SCdict
        else:
            return None
    def bank_dof(self):
        for bank in self.Filter_Banks:
            for dof in self.filter_dict[self.conf][bank]:
                setattr(self, bank+'_'+dof, self.filter_dict[self.conf][bank][dof])

# Config assignments
blend_45 = desired_conf('BLEND', 'A')
blend_90 = desired_conf('BLEND', 'B')
blend_250 = desired_conf('BLEND', 'C')

windy_SC = desired_conf('SC', 'B')
usei_SC = desired_conf('SC', 'A')

##################################################
# States

class INIT(GuardState):
    """Check the status of the blends and SC to determine which state to go to"""
    def main(self):
        self.lbm = LIGOBlendManager(deg_of_free_list=const.CHAMBER_BLEND_FILTERS_DOF, ezca=ezca)
        # A list of tuples containing the number and name respectively of the current filter
        blend_names = {dof : self.lbm.get_cur_blend_name(dof) for dof in const.CHAMBER_BLEND_FILTERS_DOF}

        ## Check if in Blend A
        # Make sure the dof sets are the same
        if set(blend_45.Blend_dofs) == set([dof for dof in blend_names]):
            # Make a new list containing the dof that has a matching engaged filter,
            # and then if the list is still just as long then move on.
            if len([dof for dof in blend_names if blend_names[dof][0] == \
                        getattr(blend_45, 'BLEND_{}'.format(dof))[0]]) \
                        == len(blend_45.Blend_dofs):
                # We are in Blend A !!!

                for dof in const.SC_ENGAGED_DOF:
                    
                    FIR_gain = ezca['SENSCOR_GND_STS_{}_FIR_GAIN'.format(dof)]
                    WNR_gain = ezca['SENSCOR_GND_STS_{}_WNR_GAIN'.format(dof)]
                    MATCH_gain = ezca['SENSCOR_GND_STS_{}_MATCH_GAIN'.format(dof)]
                    ### FIXME: This only only checks that the fisr dof matches, and then moves on!
                    if FIR_gain == 0.0 and WNR_gain == 0.0 and MATCH_gain == 0.0:
                        return 'BLEND_45MHZ_SC_NONE'
                    elif FIR_gain == 1.0 and WNR_gain == 0.0 and MATCH_gain == 1.0:
                        return 'BLEND_45MHZ_SC_WINDY'
                    elif FIR_gain == 0.0 and WNR_gain == 1.0 and MATCH_gain == 1.0:
                        return 'BLEND_45MHZ_SC_USEISM'
                    else:
                        # I guess go here???
                        log('SC does not match any known configuration, going to BLEND_45mHz_SC_NONE')
                        return 'BLEND_45MHZ_SC_NONE'
            else:
                log('CPA')

        ## Check if in Blend B
        # Make sure the dof sets are the same
        if set(blend_90.Blend_dofs) == set([dof for dof in blend_names]):
            # Make a new list containing the dof that has a matching engaged filter,
            # and then if the list is still just as long then move on.
            if len([dof for dof in blend_names if blend_names[dof][0] == \
                        getattr(blend_90, 'BLEND_{}'.format(dof))[0]]) \
                        == len(blend_90.Blend_dofs):
                # We are in Blend B !!!
                for dof in const.SC_ENGAGED_DOF:
                    
                    FIR_gain = ezca['SENSCOR_GND_STS_{}_FIR_GAIN'.format(dof)]
                    WNR_gain = ezca['SENSCOR_GND_STS_{}_WNR_GAIN'.format(dof)]
                    MATCH_gain = ezca['SENSCOR_GND_STS_{}_MATCH_GAIN'.format(dof)]
                
                    if FIR_gain == 0.0 and WNR_gain == 0.0 and MATCH_gain == 0.0:
                        return 'BLEND_QUITE_90_SC_NONE'
                    elif FIR_gain == 1.0 and WNR_gain == 0.0 and MATCH_gain == 1.0:
                        return 'BLEND_QUITE_90_SC_WINDY'
                    elif FIR_gain == 0.0 and WNR_gain == 1.0 and MATCH_gain == 1.0:
                        return 'BLEND_QUITE_90_SC_USEISM'
                    else:
                        # I guess go here???
                        log('SC does not match any known configuration, going to BLEND_Quite_90_SC_NONE')
                        return 'BLEND_QUITE_90_SC_NONE'
            else:
                log('CPB')

        ## Check if in Blend C
        # Make sure the dof sets are the same
        if set(blend_250.Blend_dofs) == set([dof for dof in blend_names]):
            # Make a new list containing the dof that has a matching engaged filter,
            # and then if the list is still just as long then move on.
            if len([dof for dof in blend_names if blend_names[dof][0] == \
                        getattr(blend_250, 'BLEND_{}'.format(dof))[0]]) \
                        == len(blend_250.Blend_dofs):
                # We are in Blend B !!!
                for dof in const.SC_ENGAGED_DOF:
                    
                    FIR_gain = ezca['SENSCOR_GND_STS_{}_FIR_GAIN'.format(dof)]
                    WNR_gain = ezca['SENSCOR_GND_STS_{}_WNR_GAIN'.format(dof)]
                    MATCH_gain = ezca['SENSCOR_GND_STS_{}_MATCH_GAIN'.format(dof)]

                    if FIR_gain == 0.0 and WNR_gain == 0.0 and MATCH_gain == 0.0:
                        return 'BLEND_QUITE_250_SC_NONE'
                    elif FIR_gain == 1.0 and WNR_gain == 0.0 and MATCH_gain == 1.0:
                        return 'BLEND_QUITE_250_SC_WINDY'
                    elif FIR_gain == 0.0 and WNR_gain == 1.0 and MATCH_gain == 1.0:
                        return 'BLEND_QUITE_250_SC_USEISM'
                    else:
                        # I guess go here???
                        log('SC does not match any known configuration, going to BLEND_Quite_250_SC_NONE')
                        return 'BLEND_QUITE_250_SC_NONE'
            else:
                log('CPB')
                
        else:
            log('Blends are not in a known configuration, going to DOWN')
            return 'DOWN'


####################
# Blend states
    # Just grab the first FM# since they should be the same anyway
SWITCH_TO_QUITE_90_BLEND = blend_states.get_basic_blend_switch_state(
    deg_of_free_list=blend_90.Blend_dofs, 
    blend_number=getattr(blend_90, 'BLEND_{}'.format(blend_90.Blend_dofs[0]))[0], 
    requestable=False)

SWITCH_TO_45MHZ_BLEND = blend_states.get_basic_blend_switch_state(
    deg_of_free_list=blend_45.Blend_dofs,
    blend_number=getattr(blend_45, 'BLEND_{}'.format(blend_45.Blend_dofs[0]))[0], 
    requestable=False)

SWITCH_TO_QUITE_250_BLEND = blend_states.get_basic_blend_switch_state(
    deg_of_free_list=blend_250.Blend_dofs,
    blend_number=getattr(blend_250, 'BLEND_{}'.format(blend_250.Blend_dofs[0]))[0], 
    requestable=False)

####################
# SC states

TURN_ON_BLEND_45_WINDY_SC = sc_states.get_SC_switch_one_bank(
    windy_SC.Filter_Banks[0],
    windy_SC.SC_dofs[windy_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_45_USEISM_SC = sc_states.get_SC_switch_one_bank(
    usei_SC.Filter_Banks[0],
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_90_WINDY_SC = sc_states.get_SC_switch_one_bank(
    windy_SC.Filter_Banks[0],
    windy_SC.SC_dofs[windy_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_90_USEISM_SC = sc_states.get_SC_switch_one_bank(
    usei_SC.Filter_Banks[0],
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_250_WINDY_SC = sc_states.get_SC_switch_one_bank(
    windy_SC.Filter_Banks[0],
    windy_SC.SC_dofs[windy_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_250_USEISM_SC = sc_states.get_SC_switch_one_bank(
    usei_SC.Filter_Banks[0],
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]],
    1,
    ramp_time=5)



# Three of the same states but with different names to make a longer path so it doesn't skip
TURN_OFF_BLEND_90_SC = sc_states.get_SC_all_gains_off(
    usei_SC.Filter_Banks + windy_SC.Filter_Banks,
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]] + windy_SC.SC_dofs[windy_SC.Filter_Banks[0]], ### FIXME: This only works while we are just using one FB, need to find a more creative way to find the dofs
    ramp_time=5)
TURN_OFF_BLEND_45_SC = sc_states.get_SC_all_gains_off(
    usei_SC.Filter_Banks + windy_SC.Filter_Banks,
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]] + windy_SC.SC_dofs[windy_SC.Filter_Banks[0]], ### FIXME: see above
    ramp_time=5)
TURN_OFF_BLEND_250_SC = sc_states.get_SC_all_gains_off(
    usei_SC.Filter_Banks + windy_SC.Filter_Banks,
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]] + windy_SC.SC_dofs[windy_SC.Filter_Banks[0]], ### FIXME: see above above
    ramp_time=5)

####################
# Idle states

def get_idle_state(index):
    class IDLE_STATE(GuardState):
        def main(self):
            return True
        def run(self):
            return True
    IDLE_STATE.index = index
    return IDLE_STATE
        

##########
# 45s
BLEND_45MHZ_SC_WINDY = get_idle_state(48)
BLEND_45MHZ_SC_USEISM = get_idle_state(47)
BLEND_45MHZ_SC_NONE = get_idle_state(45)
##########
# 90s
BLEND_QUITE_90_SC_WINDY = get_idle_state(98)
BLEND_QUITE_90_SC_USEISM = get_idle_state(97)
BLEND_QUITE_90_SC_NONE = get_idle_state(90)
###########
# 250s
BLEND_QUITE_250_SC_WINDY = get_idle_state(258)
BLEND_QUITE_250_SC_USEISM = get_idle_state(257)
BLEND_QUITE_250_SC_NONE = get_idle_state(250)
##########

class DOWN(GuardState):
    """This state is more of just a parking spot for the node to go
    when someone wants to change the configuration to something other
    than what is defined here, or if there is a problem it can jump to DOWN.
    """
    goto = True
    index = 1
    def main(self):
        return True
    def run(self):
        return True
##################################################

edges = [
    #####
    # BLEND CHANGES
    ('BLEND_45MHZ_SC_NONE', 'SWITCH_TO_QUITE_90_BLEND'),
    ('BLEND_45MHZ_SC_NONE', 'SWITCH_TO_QUITE_250_BLEND'),
    ('SWITCH_TO_45MHZ_BLEND', 'BLEND_45MHZ_SC_NONE'),

    ('BLEND_QUITE_90_SC_NONE', 'SWITCH_TO_45MHZ_BLEND'),
    ('BLEND_QUITE_90_SC_NONE', 'SWITCH_TO_QUITE_250_BLEND'),
    ('SWITCH_TO_QUITE_90_BLEND', 'BLEND_QUITE_90_SC_NONE'),

    ('BLEND_QUITE_250_SC_NONE', 'SWITCH_TO_45MHZ_BLEND'),
    ('BLEND_QUITE_250_SC_NONE', 'SWITCH_TO_QUITE_90_BLEND'),
    ('SWITCH_TO_QUITE_250_BLEND', 'BLEND_QUITE_250_SC_NONE'),
    #####
    # 45 SC
    ('BLEND_45MHZ_SC_NONE', 'TURN_ON_BLEND_45_WINDY_SC'),
    ('TURN_ON_BLEND_45_WINDY_SC', 'BLEND_45MHZ_SC_WINDY'),
    ('BLEND_45MHZ_SC_WINDY', 'TURN_OFF_BLEND_45_SC'),

    ('TURN_OFF_BLEND_45_SC', 'BLEND_45MHZ_SC_NONE'),

    ('BLEND_45MHZ_SC_NONE', 'TURN_ON_BLEND_45_USEISM_SC'),
    ('TURN_ON_BLEND_45_USEISM_SC', 'BLEND_45MHZ_SC_USEISM'),
    ('BLEND_45MHZ_SC_USEISM', 'TURN_OFF_BLEND_45_SC'),
    #####
    # 90 SC
    ('BLEND_QUITE_90_SC_NONE', 'TURN_ON_BLEND_90_WINDY_SC'),
    ('TURN_ON_BLEND_90_WINDY_SC', 'BLEND_QUITE_90_SC_WINDY'),
    ('BLEND_QUITE_90_SC_WINDY', 'TURN_OFF_BLEND_90_SC'),

    ('TURN_OFF_BLEND_90_SC', 'BLEND_QUITE_90_SC_NONE'),

    ('BLEND_QUITE_90_SC_NONE', 'TURN_ON_BLEND_90_USEISM_SC'),
    ('TURN_ON_BLEND_90_USEISM_SC', 'BLEND_QUITE_90_SC_USEISM'),
    ('BLEND_QUITE_90_SC_USEISM', 'TURN_OFF_BLEND_90_SC'),
    #####
    # 250 SC
    ('BLEND_QUITE_250_SC_NONE', 'TURN_ON_BLEND_250_WINDY_SC'),
    ('TURN_ON_BLEND_250_WINDY_SC', 'BLEND_QUITE_250_SC_WINDY'),
    ('BLEND_QUITE_250_SC_WINDY', 'TURN_OFF_BLEND_250_SC'),

    ('TURN_OFF_BLEND_250_SC', 'BLEND_QUITE_250_SC_NONE'),

    ('BLEND_QUITE_250_SC_NONE', 'TURN_ON_BLEND_250_USEISM_SC'),
    ('TURN_ON_BLEND_250_USEISM_SC', 'BLEND_QUITE_250_SC_USEISM'),
    ('BLEND_QUITE_250_SC_USEISM', 'TURN_OFF_BLEND_250_SC'),

    ]

